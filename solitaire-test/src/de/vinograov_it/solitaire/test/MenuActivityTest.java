package de.vinograov_it.solitaire.test;

import de.vinogradov_it.solitaire.SolitaireMenuActivity;
import de.vinogradov_it.solitaire.R;
import android.test.ActivityInstrumentationTestCase2;
import android.test.TouchUtils;
import android.view.KeyEvent;
import android.widget.Button;

public class MenuActivityTest extends
		ActivityInstrumentationTestCase2<SolitaireMenuActivity> {
	
	public MenuActivityTest() {
		super("de.vinogradov_it.solitaire", SolitaireMenuActivity.class);
		
	}
	
	private SolitaireMenuActivity mActivity;
	private Button mRulesButton;
	private Button mPlayButton;
	private Button mSettingsButton;
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		mActivity = getActivity();
		mRulesButton= (Button) mActivity.findViewById(R.id.rules);
		mSettingsButton= (Button) mActivity.findViewById(R.id.settings);
		mPlayButton= (Button) mActivity.findViewById(R.id.play);
	}

	public void testButtons(){
		assertTrue(mRulesButton.getText().toString().equals("Rules"));
		assertTrue(mPlayButton.getText().toString().equals("Play"));
		assertTrue(mSettingsButton.getText().toString().equals("Settings"));
	}
	
	public void testRulesActivity(){
		TouchUtils.clickView(this, mRulesButton);
		this.sendKeys(KeyEvent.KEYCODE_BACK);
		TouchUtils.clickView(this, mPlayButton);
		this.sendKeys(KeyEvent.KEYCODE_BACK);
		TouchUtils.clickView(this, mSettingsButton);
		this.sendKeys(KeyEvent.KEYCODE_BACK);
		

	}
}
