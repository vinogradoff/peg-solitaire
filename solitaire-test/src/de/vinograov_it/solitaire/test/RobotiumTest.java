package de.vinograov_it.solitaire.test;

import com.jayway.android.robotium.solo.Solo;

import de.vinogradov_it.solitaire.SolitaireMenuActivity;
import de.vinogradov_it.solitaire.SolitaireRulesActivity;

import de.vinogradov_it.solitaire.R;
import android.test.ActivityInstrumentationTestCase2;


public class RobotiumTest extends
		ActivityInstrumentationTestCase2<SolitaireMenuActivity> {

	public RobotiumTest() {
		super("de.vinogradov_it.solitaire", SolitaireMenuActivity.class);
	}
	private Solo solo;

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		solo = new Solo(getInstrumentation(), getActivity());

	}

	public void testRulesPage() throws Exception {
	    // Check that we have the right activity
	    solo.assertCurrentActivity("wrong activiy", SolitaireMenuActivity.class);

	    // Click a button which will start a new Activity
	    // Here we use the ID of the string to find the right button
	    solo.clickOnButton(solo.getString(R.string.rules));
	    // Validate that the Activity is the correct one
	    solo.assertCurrentActivity("wrong activiy", SolitaireRulesActivity.class);
	    assertTrue("Peg is in the rules text",solo.waitForText("peg"));
	    assertFalse( "Girls are not in the rules text",solo.searchText("girls"));
	  }
	
	@Override
	public void tearDown() throws Exception {
		try {
			solo.finalize();
		} catch (Throwable e) {

			e.printStackTrace();
		}
		getActivity().finish();
		super.tearDown();
	}
}