package de.vinogradov_it.solitaire;

import de.vinogradov_it.solitaire.R;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.preference.PreferenceManager;

/**
 * Helper class providing methods to other classes to obtain puzzle lists,
 * names and setups for different puzzles.
 */
public class SolitairePuzzles {
	private Context mContext = null;
	public SolitairePuzzles(Context context) {
		mContext = context;
	}
	/**
	 * 
	 * @param boardVersion current board version
	 * @return the list of puzzles (keys) for the given board version
	 */
	public String[] getPuzzles(String boardVersion) {
		Resources res = mContext.getResources();
		if (boardVersion.equalsIgnoreCase("en")) {
    		return res.getStringArray(R.array.puzzleVersions);
		}
		else if (boardVersion.equalsIgnoreCase("eu")) {
    		return res.getStringArray(R.array.puzzleVersionsEU);
		}
		else if (boardVersion.equalsIgnoreCase("de")) {
    		return res.getStringArray(R.array.puzzleVersionsDE);
		}
		
		return null;
	}
	
	public String getPuzzleTitle(String puzzleVersion, int maxStoneCount) {
		Resources res = mContext.getResources();
    	if (puzzleVersion.equalsIgnoreCase("classic")) {
    		return res.getString(R.string.classic);
    	}
    	else if (puzzleVersion.equalsIgnoreCase("random")) {
    		String title = res.getString(R.string.random);
    		
	    	String[] values = res.getStringArray(R.array.randomMaxCounts);
	    	String[] titles = res.getStringArray(R.array.randomMaxCountsTitles);
	    	String sMaxCount = Integer.toString(maxStoneCount); 
	    		
			if (values != null && titles != null) {
				int index = -1;
				for (int i = 0; i < values.length; i++) {
					if (values[i].equalsIgnoreCase(sMaxCount)) {
						index = i;
						break;
					}
				}
				
				if (index >= 0) {
		        	title += " - " + titles[index];
	        	}
			}
    		
    		return title;
    	}
    	else {
			int resId = res.getIdentifier(puzzleVersion, "string", mContext.getPackageName());
			return res.getString(resId);
    	}
	}
	
	public String getPuzzleSetup(String boardVersion, String puzzleVersion) {
    	Resources res = mContext.getResources();
    	
    	if (puzzleVersion.equalsIgnoreCase("classic")) {
    		return "";
    	}
    	else if (puzzleVersion.equalsIgnoreCase("random")) {
    		return "random"; // Board will generate a random field
    	}
    	else {
    		int resId;
    		if (boardVersion.equalsIgnoreCase("eu"))
    			resId = res.getIdentifier(puzzleVersion+"EUSetup", "string", mContext.getPackageName());
    		else if (boardVersion.equalsIgnoreCase("de"))
    			resId = res.getIdentifier(puzzleVersion+"DESetup", "string", mContext.getPackageName());
    		else
    			resId = res.getIdentifier(puzzleVersion+"Setup", "string", mContext.getPackageName());
    		
    		return res.getString(resId);
    	}
	}
	
	public boolean canSelectNextPuzzle(String boardVersion, String puzzleVersion) {
    	if (puzzleVersion.equalsIgnoreCase("random")) {
    		return true;
    	}
    	else {
			String[] puzzles = getPuzzles(boardVersion);
			if (puzzles.length <= 0 || puzzleVersion.equalsIgnoreCase(puzzles[puzzles.length-1]))
				return false;
			else
				return true;
    	}
	}

	public boolean canSelectPrevPuzzle(String boardVersion, String puzzleVersion) {
    	if (puzzleVersion.equalsIgnoreCase("random")) {
    		return true;
    	}
    	else {
			String[] puzzles = getPuzzles(boardVersion);
			if (puzzles.length <= 0 || puzzleVersion.equalsIgnoreCase(puzzles[0]))
				return false;
			else
				return true;
    	}
	}
	
	/**
	 * Returns the cound of currently solved puzzles
	 * @return integer count of solved puzzle, 0 if none solved.
	 */
	public int countSolvedPuzzles(){
		SharedPreferences prefs=PreferenceManager.getDefaultSharedPreferences(mContext);
		String solvedPuzzles=prefs.getString("solvedPuzzles", "");
        return solvedPuzzles.split(";").length;
	}
	
	/**
	 * Returns the stats of solved random puzzles
	 * @return string as n1:n2:n3:n4:n5, where n(i) - count of solved puzzles, i=1 - easy .. i=5 - expert 
	 */
	public String getRandomSolvedPuzzlesStats(){
		SharedPreferences prefs=PreferenceManager.getDefaultSharedPreferences(mContext);
		String solvedPuzzles=prefs.getString("solvedRandom", "0:0:0:0:0");
        return solvedPuzzles;
	}
}
