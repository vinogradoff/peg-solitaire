/*
 * Copyright 2012 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.vinogradov_it.solitaire;

import static de.vinogradov_it.solitaire.CommonUtilities.GCM_SENDER_ID;
import static de.vinogradov_it.solitaire.CommonUtilities.GCM_SERVER_URL;

import com.google.android.gcm.GCMRegistrar;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Build;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

/**
 * Helper class used to communicate with the demo server.
 */
public final class GCMServerUtilities {

    private static final int MAX_ATTEMPTS = 5;
    private static final int BACKOFF_MILLI_SECONDS = 2000;
    public static final long CONNECT_TO_SERVER_INTERVAL = 3*24*60*60*1000;	// in ms. =3 days
    /**
	 * Tag used on log messages.
	 */
	static final String TAG = "GCMService";
    private static final Random random = new Random();
    
	static AsyncTask<Void, Void, Void> mRegisterTask;

    /**
     * Register this account/device pair within the server with GCM.
     *
     * @return whether the registration succeeded or not.
     */
    static boolean register(final Context context, final Map<String,String> params) {
        Log.i(TAG, "registering device (regId = " + params.get("regId") + ")");
        String serverUrl = GCM_SERVER_URL + "/register";
        long backoff = BACKOFF_MILLI_SECONDS + random.nextInt(1000);
        // Once GCM returns a registration id, we need to register it in the
        // demo server. As the server might be down, we will retry it a couple
        // times.
        for (int i = 1; i <= MAX_ATTEMPTS; i++) {
            Log.d(TAG, "Attempt #" + i + " to register");
            try {
                //displayMessage(context, context.getString(R.string.server_registering, i, MAX_ATTEMPTS));
                post(serverUrl, params);
                GCMRegistrar.setRegisteredOnServer(context, true);
                //String message = context.getString(R.string.server_registered);
                //CommonUtilities.displayMessage(context, message);
                return true;
            } catch (IOException e) {
                // Here we are simplifying and retrying on any error; in a real
                // application, it should retry only on unrecoverable errors
                // (like HTTP error code 503).
                Log.e(TAG, "Failed to register on attempt " + i, e);
                if (i == MAX_ATTEMPTS) {
                    break;
                }
                try {
                    Log.d(TAG, "Sleeping for " + backoff + " ms before retry");
                    Thread.sleep(backoff);
                } catch (InterruptedException e1) {
                    // Activity finished before we complete - exit.
                    Log.d(TAG, "Thread interrupted: abort remaining retries!");
                    Thread.currentThread().interrupt();
                    return false;
                }
                // increase backoff exponentially
                backoff *= 2;
            }
        }
        //String message = context.getString(R.string.server_register_error,MAX_ATTEMPTS);
        //CommonUtilities.displayMessage(context, message);
        return false;
    }

    /**
     * Register this account/device pair within the server without GCM.
     *
     * @return whether the registration succeeded or not.
     */
    static boolean registerWithoutGCM(final Map<String,String> params) {

    	mRegisterTask = new AsyncTask<Void, Void, Void>() {
    	
        @Override
        protected Void doInBackground(Void... p) {
            boolean registered =
                    GCMServerUtilities._registerWithoutGCM(params);        
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            mRegisterTask = null;
        }

    	};
    	mRegisterTask.execute(null,null,null);
    	return true;
    }
    /**
     * Register this account/device pair within the server without GCM.
     *
     * @return whether the registration succeeded or not.
     */
    private static boolean _registerWithoutGCM(final Map<String,String> params) {
        Log.i(TAG, "registering device (regId = " + params.get("regId") + ")");
        String serverUrl = GCM_SERVER_URL + "/register";
        long backoff = BACKOFF_MILLI_SECONDS + random.nextInt(1000);
        for (int i = 1; i <= MAX_ATTEMPTS; i++) {
            Log.d(TAG, "Attempt #" + i + " to register");
            try {
                post(serverUrl, params);        
                return true;
            } catch (IOException e) {
                // Here we are simplifying and retrying on any error; in a real
                // application, it should retry only on unrecoverable errors
                // (like HTTP error code 503).
                Log.e(TAG, "Failed to register on attempt " + i, e);
                if (i == MAX_ATTEMPTS) {
                    break;
                }
                try {
                    Log.d(TAG, "Sleeping for " + backoff + " ms before retry");
                    Thread.sleep(backoff);
                } catch (InterruptedException e1) {
                    // Activity finished before we complete - exit.
                    Log.d(TAG, "Thread interrupted: abort remaining retries!");
                    Thread.currentThread().interrupt();
                    return false;
                }
                // increase backoff exponentially
                backoff *= 2;
            }
        }
        return false;
    }
    /**
     * Unregister this account/device pair within the server.
     */
    static void unregister(final Context context, final String regId) {
        Log.i(TAG, "unregistering device (regId = " + regId + ")");
        String serverUrl = GCM_SERVER_URL + "/unregister";
        Map<String, String> params = new HashMap<String, String>();
        params.put("regId", regId);
        try {
            post(serverUrl, params);
            GCMRegistrar.setRegisteredOnServer(context, false);
            //String message = context.getString(R.string.server_unregistered);
            //CommonUtilities.displayMessage(context, message);
        } catch (IOException e) {
            // At this point the device is unregistered from GCM, but still
            // registered in the server.
            // We could try to unregister again, but it is not necessary:
            // if the server tries to send a message to the device, it will get
            // a "NotRegistered" error message and should unregister the device.
            //String message = context.getString(R.string.server_unregister_error, e.getMessage());
            //CommonUtilities.displayMessage(context, message);
        }
    }

    
    /**
     * Issue a POST request to the server.
     *
     * @param endpoint POST address.
     * @param params request parameters.
     *
     * @throws IOException propagated from POST.
     */
    private static void post(String endpoint, Map<String, String> params)
            throws IOException {
        URL url;
        try {
            url = new URL(endpoint);
        } catch (MalformedURLException e) {
            throw new IllegalArgumentException("invalid url: " + endpoint);
        }
        StringBuilder bodyBuilder = new StringBuilder();
        Iterator<Entry<String, String>> iterator = params.entrySet().iterator();
        // constructs the POST body using the parameters
        while (iterator.hasNext()) {
            Entry<String, String> param = iterator.next();
            bodyBuilder.append(param.getKey()).append('=')
                    .append(param.getValue());
            if (iterator.hasNext()) {
                bodyBuilder.append('&');
            }
        }
        String body = bodyBuilder.toString();
        Log.v(TAG, "Posting '" + body + "' to " + url);
        byte[] bytes = body.getBytes();
        HttpURLConnection conn = null;
        try {
            conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setUseCaches(false);
            conn.setFixedLengthStreamingMode(bytes.length);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded;charset=UTF-8");
            // post the request
            OutputStream out = conn.getOutputStream();
            out.write(bytes);
            out.close();
            // handle the response
            int status = conn.getResponseCode();
            if (status != 200) {
              throw new IOException("Post failed with error code " + status);
            }
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }
      }

	/**
	 * register this Device in GCM; do nothing if already registered.
	 * check the for server-side registration every week.
	 * pContext - context (usually this).
	 * 
	 */
	public static void registerDeviceInGCM(Context pContext) {

		// Make sure the device has the proper dependencies.
	    GCMRegistrar.checkDevice(pContext);
	    // Make sure the manifest was properly set - comment out this line
	    // while developing the app, then uncomment it when it's ready.
	    GCMRegistrar.checkManifest(pContext);
	    // reset information every 30 secs (for developement), so that it would be resend again
	    //GCMRegistrar.setRegisterOnServerLifespan(pContext, 1*1*1*30*1000);
	    // default is 1 week, set e.g. to 1 day in production
	    GCMRegistrar.setRegisterOnServerLifespan(pContext, 3*24*60*60*1000);
	    final String regId = GCMRegistrar.getRegistrationId(pContext);
	    if (regId.equals("")) {
	        // Automatically registers application on startup.
	        GCMRegistrar.register(pContext, GCM_SENDER_ID);
	    } else {
	        // Device is already registered on GCM, check server.
	        if (GCMRegistrar.isRegisteredOnServer(pContext)) {
	            // Skips registration.
	        	//CommonUtilities.showShortToast(this, "CGM: already registered");
	        } else {
	            // Try to register again, but not in the UI thread.
	            // It's also necessary to cancel the thread onDestroy(),
	            // hence the use of AsyncTask instead of a raw thread.
	        	//CommonUtilities.showShortToast(this, "CGM: Try to register on Server again");
	            final Context context = pContext;
	            mRegisterTask = new AsyncTask<Void, Void, Void>() {
	
	                @Override
	                protected Void doInBackground(Void... params) {
	                    boolean registered =
	                            GCMServerUtilities.register(context, getDataForGCM(context));
	                    // At this point all attempts to register with the app
	                    // server failed, so we need to unregister the device
	                    // from GCM - the app will try to register again when
	                    // it is restarted. Note that GCM will send an
	                    // unregistered callback upon completion, but
	                    // GCMIntentService.onUnregistered() will ignore it.
	                    if (!registered) {
	                        GCMRegistrar.unregister(context);
	                    }
	                    return null;
	                }
	
	                @Override
	                protected void onPostExecute(Void result) {
	                    mRegisterTask = null;
	                }
	
	            };
	            mRegisterTask.execute(null, null, null);
	        }
	    }
	}

	@SuppressLint("NewApi")
	static Map<String,String> getDataForGCM(Context pContext){
		Map<String,String> params=new HashMap<String, String>();
		if (CommonUtilities.hasGoogleAPI(pContext)){
			String regId=GCMRegistrar.getRegistrationId(pContext);
			if (regId==null || regId.equals("")){
				regId=CommonUtilities.getAndroidUUID(pContext);
			}
			params.put("regId",regId);
		}else{
			params.put("regId", CommonUtilities.getAndroidUUID(pContext));
		}
		try {
			String versionName = pContext.getPackageManager().getPackageInfo(pContext.getPackageName(), 0).versionName;
			int versionCode = pContext.getPackageManager().getPackageInfo(pContext.getPackageName(), 0).versionCode;
			params.put("versionName", versionName);
			params.put("versionCode", String.valueOf(versionCode));
			params.put("androidAPI", String.valueOf(Build.VERSION.SDK_INT));
			params.put("language",Locale.getDefault().getLanguage());
			params.put("displayLanguage",Locale.getDefault().getDisplayLanguage());
			TelephonyManager tm = (TelephonyManager) pContext.getSystemService(Context.TELEPHONY_SERVICE);
			if (tm==null || tm.getSimCountryIso()==null) params.put("simCountryCode", "n/a");
			else params.put("simCountryCode", tm.getSimCountryIso());
			params.put("localeCountry", pContext.getResources().getConfiguration().locale.getCountry());
			params.put("displayLocaleCountry", pContext.getResources().getConfiguration().locale.getDisplayCountry());
			DisplayMetrics dm=pContext.getResources().getDisplayMetrics();
			params.put("densityScale",String.valueOf(dm.density));
			params.put("densityDpi",String.valueOf(dm.densityDpi));
			params.put("heightDP",String.valueOf((int)(dm.heightPixels/dm.density)));
			params.put("widthDP",String.valueOf((int)(dm.widthPixels/dm.density)));
			String screenCategory="n/a";
			if ((pContext.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) 
					== Configuration.SCREENLAYOUT_SIZE_SMALL) {
				screenCategory="small";
			}
			if ((pContext.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK)
					== Configuration.SCREENLAYOUT_SIZE_NORMAL) {
				screenCategory="normal";
			}
			if ((pContext.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK)
					== Configuration.SCREENLAYOUT_SIZE_LARGE) {
				screenCategory="large";
			}
			if (Build.VERSION.SDK_INT>=9){
				//if ((pContext.getResources().getConfiguration().screenLayout &Configuration.SCREENLAYOUT_SIZE_MASK) 
				//		== Configuration.SCREENLAYOUT_SIZE_XLARGE) {
				if (DonutsNotCompatFunctionsHelper.checkXLargeLayout(pContext)) screenCategory="xlarge";
			}
			params.put("screenCategory", screenCategory);
			params.put("hasGoogleAccount", "n/a");
			if (Build.VERSION.SDK_INT>=5){
				String[] names=DonutsNotCompatFunctionsHelper.getGoogleAccountNames(pContext);
			    if (names.length == 0){
			    	params.put("hasGoogleAccount", "false");
			    }else{
			    	params.put("hasGoogleAccount", "true");
			    	params.put("googleAccountName",names[0]);
			    }
			}
			SolitairePuzzles puzzles=new SolitairePuzzles(pContext);
		    params.put("solvedLevels", String.valueOf(puzzles.countSolvedPuzzles()));
		    params.put("randomSolvedLevels", puzzles.getRandomSolvedPuzzlesStats());
		} catch (NameNotFoundException e) {
			Log.e(TAG,"Package name not found", e);
		}
		return params;
	}
	/** Checks if we should connect to server again
	 * 
	 * @param pContext context
	 * @param period in ms, how long to wait until new connect to server
	 * @return
	 */
    static Boolean shouldConnectAgain(Context pContext, Long period){
    	SharedPreferences prefs=PreferenceManager.getDefaultSharedPreferences(pContext);
		Long lastConnect=prefs.getLong("lastConnectToServer", 0);
		Long now=new Date().getTime();
		if ( now - lastConnect<period) return false;
		else {
			SharedPreferences.Editor editor = prefs.edit();
			editor.putLong("lastConnectToServer", now);
			editor.commit();
		}
		return true;
		
    }
}
