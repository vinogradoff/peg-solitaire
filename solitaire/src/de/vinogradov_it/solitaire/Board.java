package de.vinogradov_it.solitaire;

import java.util.ArrayList;
import java.util.Random;

import de.vinogradov_it.solitaire.Cell.CellType;

import android.graphics.Point;
import android.os.Bundle;
import android.view.View;

public class Board {

	private final boolean mAllowDiagonalMoves = false;
	private Cell[][] mBoardArray = null;
	private Point mSelectedCell = new Point(-1,-1);
	private boolean mHasMoves = false;
	private ArrayList<String> mUndoMoves = new ArrayList<String>(0);
	private ArrayList<String> mRedoMoves = new ArrayList<String>(0);
	private String mInitialRandomSetup = new String();
	
	public Board(String boardVersion, String puzzleVersion, int nRandomStones) {
		int size = 7;
		boolean bEurope = false;
		if (boardVersion.equalsIgnoreCase("de"))
			size = 9;
		else
			bEurope = boardVersion.equalsIgnoreCase("eu");
	
		int middle = (size-1) / 2;
		mBoardArray = new Cell[size][size];
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				mBoardArray[i][j] = new Cell();
				if (Math.abs(i-middle) >= 2 && Math.abs(j-middle) >= 2)
				{
					if (!bEurope || Math.abs(i-middle) > 2 || Math.abs(j-middle) > 2)
						mBoardArray[i][j].setType(CellType.None);
				}
				else if (i == middle && j == middle)
					mBoardArray[i][j].setType(CellType.Empty);
			}
		}

		
		if (puzzleVersion != null && puzzleVersion != "")
		{
			if (puzzleVersion.equalsIgnoreCase("random")) {
				generateRandom(nRandomStones);
				mInitialRandomSetup = fieldToString();
			}
			else
				fieldFromString(puzzleVersion);
		}
		
		analyze();
	}
	
	public void restart() {
		if (mInitialRandomSetup != "") {
			mSelectedCell.x = -1;
			mSelectedCell.y = -1;
			mRedoMoves.clear();
			mUndoMoves.clear();
			fieldFromString(mInitialRandomSetup);
		}
	}
	
	public boolean canUndo() {
		return mUndoMoves.size() > 0;
	}
	
	public boolean canRedo() {
		return mRedoMoves.size() > 0;
	}

	public void undo() {
		int lastIndex = mUndoMoves.size()-1;
		String field = mUndoMoves.get(lastIndex);
		mRedoMoves.add(fieldToString());
		mUndoMoves.remove(lastIndex);
		fieldFromString(field);
	}
	
	public void redo() {
		int lastIndex = mRedoMoves.size()-1;
		String field = mRedoMoves.get(lastIndex);
		mUndoMoves.add(fieldToString());
		mRedoMoves.remove(lastIndex);
		fieldFromString(field);
	}
	
	private void createUndoPoint() {
		String field = fieldToString();
		mUndoMoves.add(field);
		mRedoMoves.clear();
	}
	
	public void init(Bundle inState) {
		mSelectedCell.x = -1;
		mSelectedCell.y = -1;
		String field = inState.getString("Field");
		mUndoMoves = inState.getStringArrayList("Undo");
		mRedoMoves = inState.getStringArrayList("Redo");
		fieldFromString(field);
	}

	public void save(Bundle outState) {
		outState.putString("Field", fieldToString());
		outState.putStringArrayList("Undo", mUndoMoves);
		outState.putStringArrayList("Redo", mRedoMoves);
	}
	
	private String fieldToString() {
		String field = new String();
		for (int i = 0; i < mBoardArray.length; i++) {
			for (int j = 0; j < mBoardArray[i].length; j++) {
				switch (mBoardArray[i][j].getType())
				{
				case Occupied:
					field += "O";
					break;
				case Selected:
					field += "S";
					break;
				default:
					field += " ";
					break;
				}
			}
		}
		
		return field;
	}

	public void generateRandom(int nStones)
	{
		int size = mBoardArray.length;
		int nMiddle = mBoardArray.length/2;
		
		for (int i = 0; i < size; i++) 
		{
			for (int j = 0; j < size; j++) 
			{
				if (mBoardArray[i][j].getType()!=CellType.None)
					mBoardArray[i][j].setType(CellType.Empty);
			}
		}
		
		Random randomGenerator = new Random();
//		int nBigRandomVal = randomGenerator.nextInt(10000); 
		int nCurrDir = randomGenerator.nextInt(4); //
		ArrayList<Integer> lStonesHoriz = new ArrayList<Integer>(0);
		ArrayList<Integer> lStonesVert = new ArrayList<Integer>(0);

		switch(nCurrDir)
		{
		case 0:
			// left
			lStonesHoriz.add(nMiddle-1);
			lStonesHoriz.add(nMiddle-2);
			lStonesVert.add(nMiddle);
			lStonesVert.add(nMiddle);
			break;
		case 1:
			// right
			lStonesHoriz.add(nMiddle+1);
			lStonesHoriz.add(nMiddle+2);
			lStonesVert.add(nMiddle);
			lStonesVert.add(nMiddle);
			break;
		case 2:
			// bottom
			lStonesHoriz.add(nMiddle);
			lStonesHoriz.add(nMiddle);
			lStonesVert.add(nMiddle-1);
			lStonesVert.add(nMiddle-2);
			break;
		case 3:
			// top
			lStonesHoriz.add(nMiddle);
			lStonesHoriz.add(nMiddle);
			lStonesVert.add(nMiddle+1);
			lStonesVert.add(nMiddle+2);
			break;
		}
		
		mBoardArray[lStonesHoriz.get(0)][lStonesVert.get(0)].setType(CellType.Occupied);
		mBoardArray[lStonesHoriz.get(1)][lStonesVert.get(1)].setType(CellType.Occupied);
		
		int xDirections[] = new int[4];
//		int yDirections[] = new int[4];
		int nCurrStone=0, nFailedAttemps=0, nMaxFailedAttemps=2; // can customize this
		Boolean bRandomStone=true;
		for (int i=2; i<nStones; ++i)
		{
			// choose stone. Only first stone can be chosen in case of two stones
			if (lStonesHoriz.size()<=2)
				nCurrStone = 0;
			else
			{
				if (bRandomStone)
					nCurrStone = randomGenerator.nextInt(lStonesHoriz.size());
			}
			int nX = lStonesHoriz.get(nCurrStone);
			int nY = lStonesVert.get(nCurrStone);
			// choose direction
			for (int j=0; j<4; ++j)
			{
				xDirections[j] = -1;
//				yDirections[j] = -1;
			}
			int nPossibleDirections=0;
			
			if (nX>1 && mBoardArray[nX-1][nY].isEmpty() && mBoardArray[nX-2][nY].isEmpty())
			{
				xDirections[0] = nPossibleDirections;
//				yDirections[0] = 0;
				nPossibleDirections++;
			}
			if (nX<size-2 && mBoardArray[nX+1][nY].isEmpty() && mBoardArray[nX+2][nY].isEmpty())
			{
				xDirections[1] = nPossibleDirections;
//				yDirections[1] = 0;
				nPossibleDirections++;
			}
			if (nY>1 && mBoardArray[nX][nY-1].isEmpty() && mBoardArray[nX][nY-2].isEmpty())
			{
				xDirections[2] = nPossibleDirections;
//				yDirections[2] = 1;
				nPossibleDirections++;
			}
			if (nY<size-2 && mBoardArray[nX][nY+1].isEmpty() && mBoardArray[nX][nY+2].isEmpty())
			{
				xDirections[3] = nPossibleDirections;
//				yDirections[3] = 1;
				nPossibleDirections++;
			}
			
			if (nPossibleDirections==0)
			{
				if (!bRandomStone)
				{
					if (++nCurrStone>=lStonesHoriz.size())
						break; // give up
				}
				else
					if (++nFailedAttemps>nMaxFailedAttemps)
					{
						// Cannot find a random stone => turn mode to find a first possible one
						bRandomStone = false;
						nCurrStone = 0;
					}
				i--;
				continue;
			}
			
			int nCurrDirection = randomGenerator.nextInt(nPossibleDirections), nDirection=0;
			for ( ; nDirection<4; ++nDirection)
			{
				if (xDirections[nDirection]==nCurrDirection)
					break;
			}

			bRandomStone = true;
			nFailedAttemps=0;
			lStonesHoriz.remove(nCurrStone);
			lStonesVert.remove(nCurrStone);

			mBoardArray[nX][nY].setType(CellType.Empty);
			
			switch(nDirection)
			{
			case 0:
				// left
				lStonesHoriz.add(nX-1);
				lStonesHoriz.add(nX-2);
				lStonesVert.add(nY);
				lStonesVert.add(nY);
				mBoardArray[nX-1][nY].setType(CellType.Occupied);
				mBoardArray[nX-2][nY].setType(CellType.Occupied);
				break;
			case 1:
				// right
				lStonesHoriz.add(nX+1);
				lStonesHoriz.add(nX+2);
				lStonesVert.add(nY);
				lStonesVert.add(nY);
				mBoardArray[nX+1][nY].setType(CellType.Occupied);
				mBoardArray[nX+2][nY].setType(CellType.Occupied);
				break;
			case 2:
				// bottom
				lStonesHoriz.add(nX);
				lStonesHoriz.add(nX);
				lStonesVert.add(nY-1);
				lStonesVert.add(nY-2);
				mBoardArray[nX][nY-1].setType(CellType.Occupied);
				mBoardArray[nX][nY-2].setType(CellType.Occupied);
				break;
			case 3:
				// top
				lStonesHoriz.add(nX);
				lStonesHoriz.add(nX);
				lStonesVert.add(nY+1);
				lStonesVert.add(nY+2);
				mBoardArray[nX][nY+1].setType(CellType.Occupied);
				mBoardArray[nX][nY+2].setType(CellType.Occupied);
				break;
			}
			
			
		} // for (int i=2; i<=nStones; ++i)
		
	}
	
	
	private void fieldFromString(String field) {
		mSelectedCell.x = -1;
		mSelectedCell.y = -1;
		for (int i = 0; i < mBoardArray.length; i++) {
			for (int j = 0; j < mBoardArray[i].length; j++) {
				if (mBoardArray[i][j].isInGame()) {
					char c = field.charAt(i*mBoardArray.length + j);
					if (c == 'O') {
						mBoardArray[i][j].setType(CellType.Occupied);
					}
					else if (c == 'S') {
						mBoardArray[i][j].setType(CellType.Selected);
						mSelectedCell.x = i;
						mSelectedCell.y = j;
					}
					else
						mBoardArray[i][j].setType(CellType.Empty);
				}
			}
		}
		analyze();
	}
	
	public Cell[][] getCells() {
		return mBoardArray;
	}
	
	public int countOccupiedCells() {
		int count = 0;
		for (int i = 0; i < mBoardArray.length; i++) {
			for (int j = 0; j < mBoardArray[i].length; j++) {
				if (mBoardArray[i][j].hasStone()) {
					count++;
				}
			}
		}
		return count;
	}
	
	private void analyze() {
		mHasMoves = false;
		for (int i = 0; i < mBoardArray.length; i++) {
			for (int j = 0; j < mBoardArray[i].length; j++) {
				mBoardArray[i][j].setCanMove(false);
				mBoardArray[i][j].setCanMoveTo(false);
			}
		}
		
		{
			// check where the selected stone can go
			int i = mSelectedCell.x;
			int j = mSelectedCell.y;
			if (i >= 0 && j >= 0) {
				boolean bSelectedStoneCanMove = false;
				if (i > 1 && mBoardArray[i-1][j].hasStone() && mBoardArray[i-2][j].isEmpty()) {
					// can move up
					mBoardArray[i-2][j].setCanMoveTo(true);
					bSelectedStoneCanMove = true;
				}
				if (i < mBoardArray.length-2 && mBoardArray[i+1][j].hasStone() && mBoardArray[i+2][j].isEmpty()) {
					// can move down
					mBoardArray[i+2][j].setCanMoveTo(true);
					bSelectedStoneCanMove = true;
				}
				if (j > 1 && mBoardArray[i][j-1].hasStone() && mBoardArray[i][j-2].isEmpty()) {
					// can move left
					mBoardArray[i][j-2].setCanMoveTo(true);
					bSelectedStoneCanMove = true;
				}
				if (j < mBoardArray[i].length-2 && mBoardArray[i][j+1].hasStone() && mBoardArray[i][j+2].isEmpty()) {
					// can move right
					mBoardArray[i][j+2].setCanMoveTo(true);
					bSelectedStoneCanMove = true;
				}
				
				if (!bSelectedStoneCanMove) {
					mBoardArray[mSelectedCell.x][mSelectedCell.y].setType(CellType.Occupied);
					mSelectedCell.x = -1;
					mSelectedCell.y = -1;
				}
			}
		}
			
		for (int i = 0; i < mBoardArray.length; i++) {
			for (int j = 0; j < mBoardArray[i].length; j++) {
				if (mBoardArray[i][j].getType() == CellType.Occupied
					|| mBoardArray[i][j].getType() == CellType.Selected) {
					if (i > 1 && mBoardArray[i-1][j].hasStone() && mBoardArray[i-2][j].isEmpty()) {
						// can move up
						mBoardArray[i][j].setCanMove(true);
						mHasMoves = true;
					}
					else if (i < mBoardArray.length-2 && mBoardArray[i+1][j].hasStone() && mBoardArray[i+2][j].isEmpty()) {
						// can move down
						mBoardArray[i][j].setCanMove(true);
						mHasMoves = true;
					}
					else if (j > 1 && mBoardArray[i][j-1].hasStone() && mBoardArray[i][j-2].isEmpty()) {
						// can move left
						mBoardArray[i][j].setCanMove(true);
						mHasMoves = true;
					}
					else if (j < mBoardArray[i].length-2 && mBoardArray[i][j+1].hasStone() && mBoardArray[i][j+2].isEmpty()) {
						// can move right
						mBoardArray[i][j].setCanMove(true);
						mHasMoves = true;
					}
				}
			}
		}
	}
	
	public boolean handleTouch(View v, float x, float y) {
		Point newSelectedCell = new Point(-1,-1);
		for (int i = 0; i < mBoardArray.length; i++) {
			for (int j = 0; j < mBoardArray[i].length; j++) {
				if (mBoardArray[i][j].getType() != CellType.None 
						&& mBoardArray[i][j].contains(x, y)) {
					newSelectedCell.x = i;
					newSelectedCell.y = j;
					break;
				}
			}
			if (newSelectedCell.x >= 0 && newSelectedCell.y >= 0)
				break;
		}

		if (newSelectedCell.x >= 0 && newSelectedCell.y >= 0) {
			CellType oldType = CellType.None; 
			CellType newType = mBoardArray[newSelectedCell.x][newSelectedCell.y].getType();
			if (mSelectedCell.x >= 0 && mSelectedCell.y >= 0) {
				oldType = mBoardArray[mSelectedCell.x][mSelectedCell.y].getType();
			}
			
			// if selected - deselect
			if (newType == CellType.Selected){
				mBoardArray[mSelectedCell.x][mSelectedCell.y].setType(CellType.Occupied);
				mSelectedCell = new Point(-1,-1);
				analyze();
				return true;
			}
			
			if (newType == CellType.Occupied) {
				// selecting a new stone
				if (mBoardArray[newSelectedCell.x][newSelectedCell.y].canMove()) {
					mBoardArray[newSelectedCell.x][newSelectedCell.y].setType(CellType.Selected);
					if (oldType == CellType.Selected) {
						mBoardArray[mSelectedCell.x][mSelectedCell.y].setType(CellType.Occupied);
					}
					mSelectedCell = newSelectedCell;
					analyze();
					return true;
				}
				else
					return false; // don't allow useless selection
			}
			else if (newType == CellType.Empty && oldType == CellType.Selected) {
				// try to move
				boolean bMoved = false;
				if (mSelectedCell.x == newSelectedCell.x && Math.abs(mSelectedCell.y-newSelectedCell.y) == 2) {
					if (mBoardArray[mSelectedCell.x][(mSelectedCell.y+newSelectedCell.y)/2].getType() == CellType.Occupied) {
						createUndoPoint();
						mBoardArray[mSelectedCell.x][(mSelectedCell.y+newSelectedCell.y)/2].setType(CellType.Empty);
						bMoved = true;
					}
				}
				else if (mSelectedCell.y == newSelectedCell.y && Math.abs(mSelectedCell.x-newSelectedCell.x) == 2) {
					if (mBoardArray[(mSelectedCell.x+newSelectedCell.x)/2][mSelectedCell.y].getType() == CellType.Occupied) {
						createUndoPoint();
						mBoardArray[(mSelectedCell.x+newSelectedCell.x)/2][mSelectedCell.y].setType(CellType.Empty);
						bMoved = true;
					}
				}
				else if (mAllowDiagonalMoves) {
						if (Math.abs(mSelectedCell.x-newSelectedCell.x) == 2 && Math.abs(mSelectedCell.y-newSelectedCell.y) == 2) {
							if (mBoardArray[(mSelectedCell.x+newSelectedCell.x)/2][(mSelectedCell.y+newSelectedCell.y)/2].getType() == CellType.Occupied) {
								createUndoPoint();
								mBoardArray[(mSelectedCell.x+newSelectedCell.x)/2][(mSelectedCell.y+newSelectedCell.y)/2].setType(CellType.Empty);
								bMoved = true;
							}
						}
				}
				
				if (bMoved) {
					mBoardArray[mSelectedCell.x][mSelectedCell.y].setType(CellType.Empty);
					mBoardArray[newSelectedCell.x][newSelectedCell.y].setType(CellType.Selected);
					mSelectedCell = newSelectedCell;
					analyze();
					return true;
				}
			}
		}

		return false;
	}

	public Point getSelectedCell() {
		return mSelectedCell;
	}

	public boolean hasMoves() {
		return mHasMoves;
	}
	
	public boolean centerOccupied() {
		return mBoardArray[(mBoardArray.length-1)/2][(mBoardArray.length-1)/2].hasStone(); 
	}
	
	public String toString(){
		return fieldToString();
	}
}
