/*
 * Copyright 2012 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.vinogradov_it.solitaire;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Configuration;

/**
 * Helper class used to communicate with the demo server.
 */
public final class DonutsNotCompatFunctionsHelper {

	
	static boolean checkXLargeLayout(Context pContext) {

		if ((pContext.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_XLARGE) {
			return true;
		}

		return false;
	}

	@SuppressLint("NewApi")
	static String[] getGoogleAccountNames(Context pContext) {
        //String[] names=new String[10];
		AccountManager accountManager = AccountManager.get(pContext);
		Account[] accounts = accountManager.getAccountsByType("com.google");
		if (accounts.length == 0) {
			return new String[0];
		} else {
			String[] names= new String[accounts.length];
			for (int i=0;i<accounts.length;i++){
				names[i]=accounts[i].name;
			}
			return names;
		}
	}
}
