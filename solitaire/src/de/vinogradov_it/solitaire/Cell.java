package de.vinogradov_it.solitaire;

import android.graphics.Rect;

public class Cell {
	
	public enum CellType
	{
		None, Empty, Occupied, Selected
	}

	private CellType mType = CellType.Occupied;
	private Rect mBounds = new Rect();

	private boolean mCanMove = false;
	private boolean mCanMoveTo = false;
	
	public Cell() {
	}
	
	public void setType(CellType mType) {
		this.mType = mType;
	}

	public CellType getType() {
		return mType;
	}

	public void setBounds(Rect mBounds) {
		this.mBounds = mBounds;
	}

	public Rect getBounds() {
		return mBounds;
	}
	
	public boolean contains (float x, float y) {
		return mBounds.contains((int)x, (int)y);
	}

	public boolean hasStone() {
		return (getType() == CellType.Occupied || getType() == CellType.Selected);
	}
	
	public boolean isInGame() {
		return (getType() != CellType.None);
	}

	public boolean isEmpty() {
		return (getType() == CellType.Empty);
	}
	
	public void setCanMoveTo(boolean bCanMoveTo) {
		this.mCanMoveTo = bCanMoveTo;
	}

	public boolean canMoveTo() {
		return mCanMoveTo;
	}

	public void setCanMove(boolean bCanMove) {
		this.mCanMove = bCanMove;
	}

	public boolean canMove() {
		return mCanMove;
	}
}
