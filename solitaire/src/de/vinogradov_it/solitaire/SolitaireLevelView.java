package de.vinogradov_it.solitaire;

import de.vinogradov_it.solitaire.Cell.CellType;
import de.vinogradov_it.solitaire.R;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.Paint.Style;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.ArcShape;
import android.util.AttributeSet;
import android.view.View;

public class SolitaireLevelView extends View {
	
	static final private int mGap = 2; 
	
	private boolean mSolved = false;
	private Board mBoard = null;
	private String mPuzzleVersion = null;
	private ShapeDrawable mStoneCircle = null;
	private ShapeDrawable mStoneImage = null;
	private Drawable mTickImage = null;

	public SolitaireLevelView(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		Resources res = getContext().getResources();
		
		mStoneCircle = new ShapeDrawable(new ArcShape(0,360));
		mStoneCircle.getPaint().setColor(res.getColor(R.color.FieldBorder));
		mStoneCircle.getPaint().setStyle(Style.STROKE);
		mStoneCircle.getPaint().setStrokeWidth(1);
		
		mStoneImage = new ShapeDrawable(new ArcShape(0,360));
		mStoneImage.getPaint().setColor(res.getColor(R.color.FieldBorder));
		mStoneImage.getPaint().setStyle(Style.FILL);
		
		mTickImage = res.getDrawable(R.drawable.tick);
	}

	public void setPuzzleVersion(String puzzleVersion) {
		mPuzzleVersion = puzzleVersion;
		mSolved = ((SolitaireChooseLevelActivity)getContext()).isPuzzleSolved(mPuzzleVersion);
	}

	public String getPuzzleVersion() {
		return mPuzzleVersion;
	}
	
	public void setBoard(Board board) {
		mBoard = board;
		initBoardSizes();
	}
	
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		initBoardSizes();
	}
	
	public void initBoardSizes() {
		if (mBoard == null)
			return;
		
		int availableWidth = getWidth() < getHeight() ? getWidth() : getHeight();
		
		availableWidth -= 2*mGap;

		if (availableWidth <= 0)
			return;
		
		int size = mBoard.getCells().length;
		
		int cellStep = availableWidth / size;
		int cellSize = cellStep - mGap;
		
		int y = (getHeight()-availableWidth)/2;
		
		for (int i = 0; i < size; i++) {
			int x = (getWidth()-availableWidth)/2;
			for (int j = 0; j < size; j++) {
				mBoard.getCells()[i][j].setBounds(new Rect(x+mGap/2,y+mGap/2,x+cellSize,y+cellSize));
				x += cellStep;
			}
			y += cellStep;
		}
	}
	
	public void onDraw(Canvas canvas) {
		super.onDraw(canvas);

		for (int i = 0; i < mBoard.getCells().length; i++) {
			for (int j = 0; j < mBoard.getCells()[i].length; j++) {
				if (mBoard.getCells()[i][j].isInGame()) {
					Rect bounds = mBoard.getCells()[i][j].getBounds();
					
					mStoneCircle.setBounds(bounds);
					mStoneCircle.draw(canvas);
					
					if (mBoard.getCells()[i][j].getType() == CellType.Occupied
						|| mBoard.getCells()[i][j].getType() == CellType.Selected) {
					    mStoneImage.setBounds(bounds);
					    mStoneImage.draw(canvas);
					}
				}
			}
		}
		
		if (mSolved) {
			mTickImage.setBounds(0,0,getWidth()/2, getHeight()/2);
			mTickImage.draw(canvas);
		}
	}
}
