package de.vinogradov_it.solitaire;

import de.vinogradov_it.solitaire.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.graphics.PorterDuff.Mode;


public class SolitaireMenuActivity extends Activity implements DialogInterface.OnClickListener {
    
    private final static String APP_PNAME = "de.vinogradov_it.solitaire";// Package Name

	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        Resources res = getResources();
        findViewById(R.id.play).getBackground().setColorFilter(res.getColor(R.color.FieldSelectable), android.graphics.PorterDuff.Mode.MULTIPLY);
        findViewById(R.id.playRandom).getBackground().setColorFilter(res.getColor(R.color.FieldSelectable), android.graphics.PorterDuff.Mode.MULTIPLY);
        findViewById(R.id.rules).getBackground().setColorFilter(res.getColor(R.color.FieldSelectable), android.graphics.PorterDuff.Mode.MULTIPLY);
        findViewById(R.id.settings).getBackground().setColorFilter(res.getColor(R.color.FieldSelectable), android.graphics.PorterDuff.Mode.MULTIPLY);
        findViewById(R.id.rateme).getBackground().setColorFilter(res.getColor(R.color.FieldSelectable), android.graphics.PorterDuff.Mode.MULTIPLY);
        findViewById(R.id.clearSolvedPuzzles).getBackground().setColorFilter(res.getColor(R.color.FieldSelectable), android.graphics.PorterDuff.Mode.MULTIPLY);
        
        if (Build.VERSION.SDK_INT>=8 && CommonUtilities.hasGoogleAPI(this)){
        	GCMServerUtilities.registerDeviceInGCM(this);  
        }else{
        	if (GCMServerUtilities.shouldConnectAgain(this, GCMServerUtilities.CONNECT_TO_SERVER_INTERVAL))
        		GCMServerUtilities.registerWithoutGCM(GCMServerUtilities.getDataForGCM(this));
        }
        AppRater.app_launched(this);
    }

	public void onPlay(View view) {
    	Intent intent = new Intent(this, SolitaireGameActivity.class);
    	intent.putExtra(SolitaireGameActivity.random, false);
    	startActivity(intent);
    }

    public void onPlayRandom(View view) {
    	Intent intent = new Intent(this, SolitaireGameActivity.class);
    	intent.putExtra(SolitaireGameActivity.random, true);
    	startActivity(intent);
    }

    public void onRules(View view) {
    	Intent intent = new Intent(this, SolitaireRulesActivity.class);
       	startActivity(intent);
    }

    /**
     * Redirect to the GooglePlay for Review
     * @param view
     */
    public void onRateMe(View view) {
    	Intent intent=new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + APP_PNAME));
       	startActivity(intent);
    }

    public void onSettings(View view) {
    	Intent intent = new Intent(this, SolitairePrefsActivity.class);
       	startActivity(intent);
    }

    public void onResetGameData(View view) {
		AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
		alertBuilder.setCancelable(true);
		Resources res = getResources();
		alertBuilder.setMessage(res.getString(R.string.userDataResetWarning));
		alertBuilder.setPositiveButton(res.getString(R.string.ok), this);
		alertBuilder.setNegativeButton(res.getString(R.string.cancel), null);

		AlertDialog alert = alertBuilder.create();
		// Title for AlertDialog
		alert.setTitle(res.getString(R.string.app_name));
		alert.show();
    }

    /**
     * Reset all the SharedPreferenses to default (or remove them) then show an info message about resetting.
     */
	@Override
	public void onClick(DialogInterface dialog, int which) {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		SharedPreferences.Editor editor = prefs.edit();
		editor.remove("solvedPuzzles");
		editor.remove("solvedRandom");
		editor.putString("puzzleVersion", "l"); // TODO: change default when non EN board version are added
		editor.commit();
		prefs = this.getSharedPreferences("apprater", 0);
		editor = prefs.edit();
		editor.remove("next_solved_count");
		editor.remove("date_firstlaunch");
		editor.remove("launch_count");
		editor.commit();

		Resources res = getResources();
		CommonUtilities.showLongToast(this, res.getString(R.string.resetDataMsg));
	}
}