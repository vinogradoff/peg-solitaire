package de.vinogradov_it.solitaire;

import de.vinogradov_it.solitaire.R;
import android.app.Activity;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;

public class SolitaireRulesActivity extends Activity {

	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rules);
        Resources res = getResources();
        findViewById(R.id.home).getBackground().setColorFilter(res.getColor(R.color.FieldSelectable), android.graphics.PorterDuff.Mode.MULTIPLY);
    }

    public void onHome(View view) {
    	finish();
    }
}
