/*
 * Copyright 2012 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.vinogradov_it.solitaire;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.util.UUID;

import com.google.android.gcm.GCMRegistrar;

import android.content.Context;
import android.provider.Settings.Secure;
import android.widget.Toast;

/**
 * Helper class providing methods and constants common to other classes in the
 * app.
 */
public final class CommonUtilities {

    /**
     * Base URL of the Demo Server (such as http://my_host:8080/gcm-demo)
     */
    static final String GCM_SERVER_URL = "http://vinogradov-it.de:8082/gcm-demo";

    /**
     * Google API project id registered to use GCM.
     */
    static final String GCM_SENDER_ID = "396658710618";
    
    /**
     * Shows a "toast" message for a short period of time
     * @param context context (mostly this)
     * @param text (text to show)
     */
    public static void showShortToast(Context context, String text){
    	Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
    }
    
    /**
     * Shows a "toast" message for a long period of time
     * @param context context (mostly this)
     * @param text (text to show)
     */
    public static void showLongToast(Context context, String text){
    	Toast.makeText(context, text, Toast.LENGTH_LONG).show();
    }
    
    /**
     * Checks support for Google API
     * @return true, if OS supports Google API, false if not.
     */
    public static boolean hasGoogleAPI(Context context){
    	Class c;
    	try {
			c=Class.forName("com.google.android.gcm.GCMRegistrar");
		} catch (ClassNotFoundException e) {
			return false;
		}
    	try{
    		Method m=c.getDeclaredMethod("checkDevice", Context.class);
    		m.invoke(null, context);
    	}catch (Exception ex){
    		return false;
    	}
    	
    	return true;
    }
    /**
     * Returns unique device ID.
     * @param context
     * @return device id as String.
     */
   public static String getAndroidUUID(Context context){
	  String id= Secure.getString(context.getContentResolver(), Secure.ANDROID_ID);
	  String uuid=null;
	  if (id!=null){
		try {
			uuid = UUID.nameUUIDFromBytes(id.getBytes("utf8")).toString();
		} catch (UnsupportedEncodingException e) {
			uuid="error_uuid";
		}
	  }
	if (uuid==null || uuid.equals("")) uuid="emulator_without_uuid";
		  //TODO Solve  problems with API 8 (2.2) (non unique id)
	  return uuid;
   }
}
