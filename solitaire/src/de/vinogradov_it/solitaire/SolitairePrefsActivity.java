package de.vinogradov_it.solitaire;

import de.vinogradov_it.solitaire.R;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.preference.Preference.OnPreferenceChangeListener;

public class SolitairePrefsActivity extends PreferenceActivity implements OnPreferenceChangeListener {
	
	@Override
	public void onCreate(Bundle savedInstanceState) {     
	    super.onCreate(savedInstanceState);        
	    addPreferencesFromResource(R.xml.prefs);
	    ListPreference pref = (ListPreference)findPreference("boardVersion");
	    if (pref != null) {
	    	pref.setOnPreferenceChangeListener(this);
	    	onPreferenceChange(pref, pref.getValue());
	    }
	}
	
	public boolean onPreferenceChange(Preference preference, Object newValue) {
		try {
			if (newValue.toString().equalsIgnoreCase("en")
				||newValue.toString().equalsIgnoreCase("eu")
				||newValue.toString().equalsIgnoreCase("de")) {
				SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
				String puzzleVersion = prefs.getString("puzzleVersion", "l"); // TODO: change default when non EN board version are added
				SolitairePuzzles puzzleUtils = new SolitairePuzzles(this);
				boolean bPuzzleExists = false;
				String[] puzzles = puzzleUtils.getPuzzles(newValue.toString());
				for (String puzzle : puzzles) {
					if (puzzleVersion.equalsIgnoreCase(puzzle)) {
						bPuzzleExists = true;
						break;
					}
				}
				
				if (!bPuzzleExists && puzzles.length > 0) {
					SharedPreferences.Editor editor = prefs.edit();
					editor.putString("puzzleVersion", puzzles[0]);
					editor.commit();
				}
			}
		}
		catch (Exception e) {
		}
		return true;
	}
}
