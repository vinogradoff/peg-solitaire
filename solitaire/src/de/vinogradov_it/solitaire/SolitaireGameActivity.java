package de.vinogradov_it.solitaire;

import de.vinogradov_it.solitaire.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.KeyEvent;
import android.view.Menu;
//import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

public class SolitaireGameActivity extends Activity {
	static public final String random = "random";
	Board mBoard = null;
	String mBoardVersion = null;
	String mBoardSetup = null;
	int mRandomStonesMaxCount = 8;
	boolean mPlayRandomPuzzle = false;
	
	public void createBoard()
	{
		mBoard = new Board(mBoardVersion, mBoardSetup, mRandomStonesMaxCount);
	}
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		mBoardVersion = prefs.getString("boardVersion", "en");
		mRandomStonesMaxCount = Integer.parseInt(prefs.getString("randomMaxCount", "8"));

		mPlayRandomPuzzle = getIntent().getBooleanExtra(random, false);
		if (mPlayRandomPuzzle) {
			setContentView(R.layout.game_random);
			initBoardSetup("random");
		}
		else {
			setContentView(R.layout.game);
			initBoardSetup(prefs.getString("puzzleVersion", "l")); // TODO: change default when non EN board version are added
		}
		
        if (savedInstanceState != null) {
        	boolean state = savedInstanceState.getBoolean("Active");
        	if (state) {
        		createBoard();
	        	mBoard.init(savedInstanceState);
        	}
        }
        
    	SolitaireGameView gameView = (SolitaireGameView)findViewById(R.id.gameSurfaceView);
    	if (gameView != null) {
    		if (mBoard == null) {
    			createBoard();
    		}
        	gameView.setActivity(this);
        	gameView.setBoard(mBoard);
        	gameView.setUndoRedoButtonStates();
    	}
    }

    @Override
    public void onPause() {
        super.onPause();
    	SolitaireGameView gameView = (SolitaireGameView)findViewById(R.id.gameSurfaceView);
    	if (gameView != null) {
    		//gameView.cleanUp();
    	}
    }
    
    @Override
    protected void onSaveInstanceState (Bundle outState) {
    	super.onSaveInstanceState(outState);
    	if (mBoard == null)
    		outState.putBoolean("Active", false);
    	else {
    		outState.putBoolean("Active", true);
       		mBoard.save(outState);
    	}
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        MenuInflater inflater = getMenuInflater();
//        inflater.inflate(R.menu.game_menu, menu);
        return true;
    }
    
    @Override
    public boolean onPrepareOptionsMenu (Menu menu) {
/*    	MenuItem undoItem = menu.findItem(R.id.undo);
    	if (undoItem != null)
    		undoItem.setEnabled(mBoard.canUndo());
    	MenuItem redoItem = menu.findItem(R.id.redo);
    	if (redoItem != null)
    		redoItem.setEnabled(mBoard.canRedo());
    	MenuItem newGameItem = menu.findItem(R.id.newGame);
    	if (newGameItem != null)
    		newGameItem.setEnabled(mBoard.canUndo());
//*/    		
    	return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
    	int itemId = item.getItemId();
        switch (itemId) {
        case R.id.home:
        case R.id.newGame:
        case R.id.nextPuzzle:
        	leaveCurrentGame(itemId);
            return true;
        case R.id.undo:
        	undo();
            return true;
        case R.id.redo:
        	redo();
            return true;
        default:
            return super.onOptionsItemSelected(item);
        }
    }
    
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
/*        if (keyCode == KeyEvent.KEYCODE_BACK) {
        	leaveCurrentGame(R.id.home);
            return true;
        }
//*/        
        return super.onKeyDown(keyCode, event);
    }

    public void newGame() {
    	SolitaireGameView gameView = (SolitaireGameView)findViewById(R.id.gameSurfaceView);
    	if (gameView != null) {

    		if (mPlayRandomPuzzle)
    			mBoard.restart();
    		else
        		createBoard();

        	gameView.setBoard(mBoard);
        	gameView.setUndoRedoButtonStates();
        	gameView.invalidate();
    	}
    }
    
    /**
     * @param forward pass false to step to the previous puzzle
     */
    public void nextPuzzle(boolean forward) {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		String puzzleVersion = prefs.getString("puzzleVersion", "l"); // TODO: change default when non EN board version are added
		if (!mPlayRandomPuzzle) {
			SolitairePuzzles puzzleUtils = new SolitairePuzzles(this); 
	    	String[] values = puzzleUtils.getPuzzles(mBoardVersion);

			if (values != null) {
				int index = -1;
				for (int i = 0; i < values.length; i++) {
					if (values[i].equalsIgnoreCase(puzzleVersion)) {
						index = i;
						break;
					}
				}
				
				if (index >= 0) {
					
					if (forward) {
						index = (index + 1) % values.length;
					}
					else {
						index = (values.length + index - 1) % values.length;
					}
					
					puzzleVersion = values[index];
					SharedPreferences.Editor editor = prefs.edit();
					editor.putString("puzzleVersion", puzzleVersion);
					editor.commit();
					initBoardSetup(puzzleVersion);
	        	}
			}
		}
		
    	SolitaireGameView gameView = (SolitaireGameView)findViewById(R.id.gameSurfaceView);
    	if (gameView != null) {
    		createBoard();
        	gameView.setBoard(mBoard);
        	gameView.setUndoRedoButtonStates();
        	gameView.invalidate();
    	}
    }
    
    private void undo() {
    	mBoard.undo();
    	View gameView = findViewById(R.id.gameSurfaceView);
    	if (gameView != null)
    		((SolitaireGameView) gameView).setUndoRedoButtonStates();
    		gameView.invalidate();
    }
    
    private void redo () {
    	mBoard.redo();
    	View gameView = findViewById(R.id.gameSurfaceView);
    	if (gameView != null)
    		((SolitaireGameView) gameView).setUndoRedoButtonStates();
    		gameView.invalidate();
    }
    
    private void initBoardSetup (String puzzleVersion) {
    	Resources res = getResources();
    	String title = ""; //res.getString(R.string.app_name);
    	
    	SolitairePuzzles puzzles = new SolitairePuzzles(this);
    	title += puzzles.getPuzzleTitle(puzzleVersion, mRandomStonesMaxCount);
    	mBoardSetup = puzzles.getPuzzleSetup(mBoardVersion, puzzleVersion);

		View next = findViewById(R.id.nextPuzzle);
		if (next != null) {
			next.setEnabled(puzzles.canSelectNextPuzzle(mBoardVersion, puzzleVersion));
		}
		
		View prev = findViewById(R.id.prevPuzzle);
		if (prev != null) {
			prev.setEnabled(puzzles.canSelectPrevPuzzle(mBoardVersion, puzzleVersion));
		}
    	
    	setTitle(title);
    }
    
    private class LeaveGameListener implements DialogInterface.OnClickListener {
    	int cmdId;
    	SolitaireGameActivity mActivity = null;
    	
    	public LeaveGameListener(SolitaireGameActivity activity, int cmdId) {
    		this.cmdId = cmdId;
    		mActivity = activity;
    	}
    	
    	public void onClick(DialogInterface dialog, int id) {
	        switch (cmdId) {
	        case R.id.home:
	        	mActivity.finish();
	            break;
	        case R.id.newGame:
	        	mActivity.newGame();
	        	break;
	        case R.id.nextPuzzle:
	        	mActivity.nextPuzzle(true);
	        	break;
	        }
		}
    }
    
    private void leaveCurrentGame(int cmdId) {
    	LeaveGameListener listener = new LeaveGameListener(this, cmdId);
    	if (mBoard.canUndo() && mBoard.hasMoves()) {
    		// need a warning
			AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
			alertBuilder.setCancelable(false);
			Resources res = getResources();
			alertBuilder.setMessage(res.getString(R.string.gameResetWarning));
			alertBuilder.setPositiveButton(res.getString(R.string.ok), listener);
			alertBuilder.setNegativeButton(res.getString(R.string.cancel), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) { }
			});

			AlertDialog alert = alertBuilder.create();
			// Title for AlertDialog
			alert.setTitle(res.getString(R.string.app_name));
			alert.show();
    	}
    	else
    		listener.onClick(null, 0);
    }

    public void onUndo(View view) {
    	undo();
    }

    public void onRedo(View view) {
    	redo();
    }
    
    public void onNewGame(View view) {
    	newGame();
    }
    
    static final int PICK_LEVEL_REQUEST = 1;
    
    private class PickRandomDifficultyListener implements DialogInterface.OnClickListener {
    	SolitaireGameActivity mActivity = null;
    	
    	public PickRandomDifficultyListener(SolitaireGameActivity activity) {
    		mActivity = activity;
    	}
    	
    	public void onClick(DialogInterface dialog, int id) {
			SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mActivity);
			Resources res = mActivity.getResources();    	            	   
			String[] stoneCounts = res.getStringArray(R.array.randomMaxCounts);
			mActivity.mRandomStonesMaxCount = Integer.parseInt(stoneCounts[id]);
			SharedPreferences.Editor editor = prefs.edit();
			editor.putString("randomMaxCount", stoneCounts[id]);
			editor.commit();
			
			mActivity.initBoardSetup("random");
			
			SolitaireGameView gameView = (SolitaireGameView)mActivity.findViewById(R.id.gameSurfaceView);
			if (gameView != null) {
				createBoard();
				gameView.setBoard(mBoard);
				gameView.setUndoRedoButtonStates();
				gameView.invalidate();
			}
		}
    }

    public void onChoosePuzzle(View view) {
    	if (mPlayRandomPuzzle) {
    		AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
    		alertBuilder.setTitle(R.string.pickDifficulty);
    		alertBuilder.setItems(R.array.randomMaxCountsTitles, new PickRandomDifficultyListener(this));

    	    AlertDialog alert = alertBuilder.create();
			alert.show();
    	}
    	else {
	    	Intent intent = new Intent(this, SolitaireChooseLevelActivity.class);
	       	startActivityForResult(intent, PICK_LEVEL_REQUEST);
    	}
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    	if (requestCode == PICK_LEVEL_REQUEST
    		&& resultCode == RESULT_OK) {

    		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
    		
    		String puzzleVersion = data.getExtras().getString(SolitaireChooseLevelActivity.level); 
    		SharedPreferences.Editor editor = prefs.edit();
			editor.putString("puzzleVersion", puzzleVersion);
			editor.commit();
    		
    		initBoardSetup(puzzleVersion);
            
        	SolitaireGameView gameView = (SolitaireGameView)findViewById(R.id.gameSurfaceView);
        	if (gameView != null) {
        		createBoard();
            	gameView.setBoard(mBoard);
            	gameView.setUndoRedoButtonStates();
            	gameView.invalidate();
            }
    	}
    }
    
    public void onNextPuzzle(View view) {
    	nextPuzzle(true);
    }

    public void onPrevPuzzle(View view) {
    	nextPuzzle(false);
    }
}
