package de.vinogradov_it.solitaire;

import java.util.StringTokenizer;

import de.vinogradov_it.solitaire.Cell.CellType;
import de.vinogradov_it.solitaire.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.Paint.Style;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.ArcShape;
import android.media.AudioManager;
import android.media.SoundPool;
import android.media.SoundPool.OnLoadCompleteListener;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.Paint.Align;
import android.media.MediaPlayer;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

public class SolitaireGameView extends View implements OnTouchListener {
	
	static final private int mGap = 4; 
	
	private boolean mNeedSound = false;
	private boolean mNeedVibration = false;
	private boolean mNeedHighlight = true;
	
	private SoundPool soundPool;
	private AudioManager audioManager;
	private int mClickSound;
	private int mWinSound;
	private int mLoseSound;
	
	private SolitaireGameActivity mActivity = null;
	private Board mBoard = null;
	private Drawable mStoneSelImage = null;
	private Drawable mStoneImage = null;
	private ShapeDrawable mStoneCircle = null;
	private ShapeDrawable mFieldSelectableCircle = null;
	private ShapeDrawable mFieldMovableToCircle = null;
	private Paint mStoneCountPainter = null;
	private Drawable mTickImage = null;
	private boolean mSolved = false;

	public SolitaireGameView(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.setOnTouchListener(this);
		Resources res = context.getResources();
		mStoneSelImage = res.getDrawable(R.drawable.stone_sel);

		mStoneCircle = new ShapeDrawable(new ArcShape(0,360));
		mStoneCircle.getPaint().setColor(res.getColor(R.color.FieldBorder));
		mStoneCircle.getPaint().setStyle(Style.STROKE);
		mStoneCircle.getPaint().setStrokeWidth(1);
		
		mFieldSelectableCircle = new ShapeDrawable(new ArcShape(0,360));
		mFieldSelectableCircle.getPaint().setColor(res.getColor(R.color.FieldSelectable));
		mFieldSelectableCircle.getPaint().setStyle(Style.FILL);
	
		mFieldMovableToCircle = new ShapeDrawable(new ArcShape(0,360));
		mFieldMovableToCircle.getPaint().setColor(res.getColor(R.color.FieldMovableTo));
		mFieldMovableToCircle.getPaint().setStyle(Style.FILL);
	
	    mStoneImage = res.getDrawable(R.drawable.stone);
	    
		mStoneCountPainter = new Paint();
		mStoneCountPainter.setColor(res.getColor(R.color.StoneCount));
		mStoneCountPainter.setAntiAlias(true);
		mStoneCountPainter.setStrokeWidth(5);
		mStoneCountPainter.setStrokeCap(Paint.Cap.ROUND);
		mStoneCountPainter.setTypeface(Typeface.create(Typeface.SERIF, Typeface.BOLD));
		mStoneCountPainter.setTextAlign(Align.CENTER);

		mTickImage = res.getDrawable(R.drawable.tick);
		
	    // initialise
	    ((Activity)context).setVolumeControlStream(AudioManager.STREAM_MUSIC);
	    // Load the sounds
	    soundPool = new SoundPool(10, AudioManager.STREAM_MUSIC, 0);
	    mWinSound = soundPool.load((Activity)context, R.raw.win, 2);
	    mClickSound = soundPool.load((Activity)context, R.raw.click, 1);
	    mLoseSound = soundPool.load((Activity)context, R.raw.lose, 3);
	    
	    audioManager=(AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
	}

	public void setBoard(Board board) {
		mBoard = board;
		initBoardSizes();
		mSolved = isPuzzleSolved();
	}
	
	public void setActivity(SolitaireGameActivity solitaireGameActivity) {
		mActivity = solitaireGameActivity;
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mActivity);
		mNeedSound = prefs.getBoolean("sound", true);
		mNeedVibration = prefs.getBoolean("vibrate", false);
		mNeedHighlight = prefs.getBoolean("highlight", true);
	}
	
	private void playSound(int id) {
		if (mNeedSound) {
			try {
				 // Getting the user sound settings
			      float actualVolume = (float) audioManager
			          .getStreamVolume(AudioManager.STREAM_MUSIC);
			      float maxVolume = (float) audioManager
			          .getStreamMaxVolume(AudioManager.STREAM_MUSIC);
			      float volume = actualVolume / maxVolume;
				switch (id) {
				case R.raw.click:
					soundPool.play(mClickSound, volume, volume, 1, 0, 1f);
					break;
				case R.raw.win:
					soundPool.play(mWinSound, volume, volume, 1, 0, 1f);
					break;
				case R.raw.lose:
					soundPool.play(mLoseSound, volume, volume, 1, 0, 1f);
					break;
				default:
					break;
				}
				
				
			}
			catch(Exception e) {
			}
		}
	}

	private void vibrate() {
		if (mNeedVibration) {
			try {
				Vibrator vib = (Vibrator) getContext().getSystemService(Context.VIBRATOR_SERVICE);
				if (vib != null)
					vib.vibrate(40);
			}
			catch(Exception e) {
			}
		}
	}
	

	public void setUndoRedoButtonStates() {
		if (mBoard != null) {
			View undo = mActivity.findViewById(R.id.undo);
			if (undo != null) {
				undo.setEnabled(mBoard.canUndo());
			}
			View redo = mActivity.findViewById(R.id.redo);
			if (redo != null) {
				redo.setEnabled(mBoard.canRedo());
			}
			View restart = mActivity.findViewById(R.id.newGame);
			if (restart != null) {
				restart.setEnabled(mBoard.canUndo());
			}
		}
	}
	
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		initBoardSizes();
	}
	
	public void initBoardSizes() {
		if (mBoard == null)
			return;
		
		int availableWidth = getWidth() < getHeight() ? getWidth() : getHeight();
		
		availableWidth -= 2*mGap;

		if (availableWidth <= 0)
			return;

		int stoneSize = mStoneImage.getIntrinsicWidth();
		
		int size = mBoard.getCells().length;
		
		int cellStep = availableWidth / size;
		int cellSize = cellStep - mGap;
		
		if (cellSize > stoneSize) {
			// use stone intrinsic sizes to avoid scaling
			cellSize = stoneSize;
			cellStep = cellSize + mGap;
			availableWidth = cellStep * size; 
		}
		
		int y = (getHeight()-availableWidth)/2;
		
		for (int i = 0; i < size; i++) {
			int x = (getWidth()-availableWidth)/2;
			for (int j = 0; j < size; j++) {
				mBoard.getCells()[i][j].setBounds(new Rect(x+mGap/2,y+mGap/2,x+cellSize,y+cellSize));
				x += cellStep;
			}
			y += cellStep;
		}
	}
	
	public void onDraw(Canvas canvas) {
		super.onDraw(canvas);

		if (mBoard.getCells().length > 0) {
			Rect bounds = mBoard.getCells()[0][0].getBounds();
			mFieldMovableToCircle.setBounds(bounds);
			mFieldMovableToCircle.draw(canvas);
			mStoneCircle.setBounds(bounds);
			mStoneCircle.draw(canvas);
			mStoneCountPainter.setTextSize(bounds.height() / 2);
			
			String text = String.valueOf(mBoard.countOccupiedCells());
			Rect textBounds = new Rect();
			mStoneCountPainter.getTextBounds(text, 0, text.length(), textBounds);
			
			canvas.drawText(text, bounds.centerX(), bounds.centerY() + textBounds.height() / 2, mStoneCountPainter);
			
			
			if (mSolved) {
				mTickImage.setBounds(mBoard.getCells()[0][1].getBounds());
				mTickImage.draw(canvas);
			}
		}
		
		for (int i = 0; i < mBoard.getCells().length; i++) {
			for (int j = 0; j < mBoard.getCells()[i].length; j++) {
				if (mBoard.getCells()[i][j].isInGame()) {
					Rect bounds = mBoard.getCells()[i][j].getBounds();
					
					mStoneCircle.setBounds(bounds);
					mStoneCircle.draw(canvas);
					
					if (mNeedHighlight) {
						if (mBoard.getCells()[i][j].canMove()) {
							mFieldSelectableCircle.setBounds(bounds);
							mFieldSelectableCircle.draw(canvas);
						}
						else if (mBoard.getCells()[i][j].canMoveTo()) {
							mFieldMovableToCircle.setBounds(bounds);
							mFieldMovableToCircle.draw(canvas);
						}
					}
					
					if (mBoard.getCells()[i][j].getType() == CellType.Occupied) {
					    mStoneImage.setBounds(bounds.left+mGap/2,bounds.top+mGap/2,bounds.right-mGap/2,bounds.bottom-mGap/2);
					    mStoneImage.draw(canvas);
					}
					else if (mBoard.getCells()[i][j].getType() == CellType.Selected) {
						mStoneSelImage.setBounds(bounds.left+mGap/2,bounds.top+mGap/2,bounds.right-mGap/2,bounds.bottom-mGap/2);
						mStoneSelImage.draw(canvas);
					}
				}
			}
		}
	}
	
	private boolean isPuzzleSolved() {
		if (mActivity.mPlayRandomPuzzle)
			return false;
		else {
	        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
	        
	        String puzzleVersion = prefs.getString("puzzleVersion", "l"); // TODO: change default when non EN board version are added
	        String solvedPuzzles = prefs.getString("solvedPuzzles", "");
	        String[] puzzles = solvedPuzzles.split(";");
	        boolean bAlreadySolved = false;
	        for (int i = 0; i < puzzles.length; i++) {
	        	if (puzzles[i].equals(puzzleVersion)) {
	        		bAlreadySolved = true;
	        		break;
	        	}
	        }
	        
	        return bAlreadySolved;
		}
	}
	
	public boolean onTouch(View v, MotionEvent event) {
		if (event.getAction() != MotionEvent.ACTION_DOWN) {
            return true;
        }		

		if (mBoard.handleTouch(v, event.getX(), event.getY())) {
			playSound(R.raw.click);
			vibrate();
			setUndoRedoButtonStates();
			invalidate();

			if (!mBoard.hasMoves()) {
				int stonesLeft = mBoard.countOccupiedCells();
				AlertDialog.Builder alertBuilder = new AlertDialog.Builder(getContext());
				alertBuilder.setCancelable(false);
				Resources res = getContext().getResources();
				// current winning condition is "1 peg left anywhere"
				if (stonesLeft > 1/* || !mBoard.centerOccupied()*/) {
					playSound(R.raw.lose);
					alertBuilder.setMessage(res.getString(R.string.youLost));
					alertBuilder.setPositiveButton(res.getString(R.string.ok), new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							// Fix: do not restart, if lost.
							//mActivity.newGame();
						}
					});
				}
				else {
					playSound(R.raw.win);
					alertBuilder.setMessage(res.getString(R.string.youWon));
					alertBuilder.setPositiveButton(res.getString(R.string.ok), new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							mActivity.nextPuzzle(true);
						}
					});
					
			        if (!mSolved) {
				        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
				        
				        String puzzleVersion = prefs.getString("puzzleVersion", "l"); // TODO: change default when non EN board version are added
				        String solvedPuzzles = prefs.getString("solvedPuzzles", "");

				        solvedPuzzles += puzzleVersion + ";";

			        	SharedPreferences.Editor editor = prefs.edit();
						editor.putString("solvedPuzzles", solvedPuzzles);
						editor.commit();
			        }
			        
			        // put statistics about played games, if random.
			        if (mActivity.mPlayRandomPuzzle){
				        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
			        	String solvedRandomPuzzles = prefs.getString("solvedRandom","0:0:0:0:0"); //5 levels
			        	StringTokenizer stringTokenizer = new StringTokenizer(solvedRandomPuzzles, ":");
			        	StringBuilder newSolvedRandom = new StringBuilder("");
			        	String[] stoneCounts = res.getStringArray(R.array.randomMaxCounts);
			        	int i=0;
			        	while (stringTokenizer.hasMoreElements()) {
							String winCount = (String) stringTokenizer.nextElement();
							if (mActivity.mRandomStonesMaxCount==Integer.valueOf(stoneCounts[i]))
								winCount=String.valueOf(Integer.valueOf(winCount).intValue()+1);
							newSolvedRandom=newSolvedRandom.append(winCount).append(":");
							i++;
						}
			        	newSolvedRandom.deleteCharAt(newSolvedRandom.length()-1);
			        	SharedPreferences.Editor editor = prefs.edit();
						editor.putString("solvedRandom", newSolvedRandom.toString());
						editor.commit();
						//CommonUtilities.showLongToast(getContext(), newSolvedRandom.toString());
			        }
				}
					
				AlertDialog alert = alertBuilder.create();
				// Title for AlertDialog
				alert.setTitle(res.getString(R.string.app_name));
				alert.show();
			}
		}

		return true;
	}
}
