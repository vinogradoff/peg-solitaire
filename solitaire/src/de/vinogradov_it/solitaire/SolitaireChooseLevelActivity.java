package de.vinogradov_it.solitaire;

import java.util.Set;
import java.util.TreeSet;

import de.vinogradov_it.solitaire.R;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.TextView;

public class SolitaireChooseLevelActivity extends Activity implements OnItemClickListener {
	static public final String level = "level";
	private Set<String> mSolvedPuzzles = new TreeSet<String>();
	
	private class LevelsAdapter extends ArrayAdapter<String> {

		private String mBoardVersion = null;
	    public LevelsAdapter(Context context, int textViewResourceId, String[] items, String boardVersion) {
	        super(context, textViewResourceId, items);
	        mBoardVersion = boardVersion; 
	    }

	    @Override
	    public View getView(int position, View convertView, ViewGroup parent) {
	        View v = convertView;
	        if (v == null) {
	            LayoutInflater vi = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	            v = vi.inflate(R.layout.level_item, null);
	        }

	        String puzzleVersion = this.getItem(position);
	        if (puzzleVersion != null) {
	            SolitaireLevelView levelImage = (SolitaireLevelView)v.findViewById(R.id.levelItemImageView);
	            if (levelImage != null) {
	            	
	            	levelImage.setPuzzleVersion(puzzleVersion);

	            	SolitairePuzzles puzzles = new SolitairePuzzles(getContext());
            		String title = puzzles.getPuzzleTitle(puzzleVersion, 10/*never used*/);
            		String boardSetup =puzzles.getPuzzleSetup(mBoardVersion, puzzleVersion); 

		            TextView levelText = (TextView)v.findViewById(R.id.levelItemTextView);
		            if (levelText != null) {
		            	levelText.setText(title);
		            }

		            levelImage.setBoard(new Board(mBoardVersion, boardSetup, 10));
	            }
	        }

	        return v;
	    }
	}	
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.level);
        
        GridView levelsView = (GridView) findViewById( R.id.levelsView);  
        
        Resources res = getResources();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        
        mSolvedPuzzles.clear();
        String solvedPuzzles = prefs.getString("solvedPuzzles", "");
        String[] puzzles = solvedPuzzles.split(";");
        for (int i = 0; i < puzzles.length; i++) {
        	mSolvedPuzzles.add(puzzles[i]);
        }
        
		String boardVersion = prefs.getString("boardVersion", "en");
		
		SolitairePuzzles puzzleUtils = new SolitairePuzzles(this);
    	String[] values = puzzleUtils.getPuzzles(boardVersion);
		
        // Create ArrayAdapter using the planet list.  
        LevelsAdapter listAdapter = new LevelsAdapter(this, R.layout.level_item, values, boardVersion);  
          
        // Set the ArrayAdapter as the ListView's adapter.  
        levelsView.setAdapter( listAdapter );
        
        levelsView.setOnItemClickListener(this);
        
        String title = res.getString(R.string.app_name);
        title += " - " + res.getString(R.string.selectLevel);
        setTitle(title);
    }

    public void onSelectLevel(String puzzleVersion) {
        Intent intent = this.getIntent();
        intent.putExtra(level, puzzleVersion);
        this.setResult(RESULT_OK, intent);
        finish();
    }

    public boolean isPuzzleSolved(String puzzleVersion) {
    	return mSolvedPuzzles.contains(puzzleVersion);
    }
    
	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		String boardVersion = prefs.getString("boardVersion", "en");
		SolitairePuzzles puzzleUtils = new SolitairePuzzles(this);
    	String[] values = puzzleUtils.getPuzzles(boardVersion);
		onSelectLevel(values[arg2]);
	}
}
