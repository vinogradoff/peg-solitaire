package de.vinogradov_it.solitaire;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.net.Uri;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class AppRater {

    private final static String APP_PNAME = "de.vinogradov_it.solitaire";// Package Name

    private final static int DAYS_UNTIL_PROMPT = 7;//Min number of days
    private final static int LAUNCHES_UNTIL_PROMPT = 20;//Min number of launches
    private final static int COUNT_OF_SOLVED_LEVELS = 5;//Min number of solved puzzles
    private final static int ADD_SOLVED_COUNT_ON_POSTPONE = 4;// If user clicks on "remind me later", then the next message will pop-up if X more puzzle solved.
    
    public static void app_launched(Context mContext) {
    	
        SharedPreferences prefs = mContext.getSharedPreferences("apprater", 0);
        if (prefs.getBoolean("dontshowagain", false)) { return ; }

        SharedPreferences.Editor editor = prefs.edit();

        // Increment launch counter
        long launch_count = prefs.getLong("launch_count", 0) + 1;
        editor.putLong("launch_count", launch_count);

        // Get date of first launch
        Long date_firstLaunch = prefs.getLong("date_firstlaunch", 0);
        if (date_firstLaunch == 0) {
            date_firstLaunch = System.currentTimeMillis();
            editor.putLong("date_firstlaunch", date_firstLaunch);
        }

        // next_solve_count is at first = COUNT_OF_SOLVED_LEVELS, but can be bigger, if "remind me later" used.
        long solved_count = prefs.getLong("next_solved_count", COUNT_OF_SOLVED_LEVELS);
        // get count of solved levels
        int solvedLevels=new SolitairePuzzles(mContext).countSolvedPuzzles();
        
        // Wait at least n days before opening
        if (launch_count >= LAUNCHES_UNTIL_PROMPT || solvedLevels>=solved_count) {
            if (System.currentTimeMillis() >= date_firstLaunch + 
                    (DAYS_UNTIL_PROMPT * 24 * 60 * 60 * 1000)) {
                showRateDialog(mContext, editor);
            }
        }

        editor.commit();
    }   

    public static void showRateDialog(final Context mContext, final SharedPreferences.Editor editor) {
    	Resources res=mContext.getResources();
    	
    	final Dialog dialog = new Dialog(mContext);
        dialog.setTitle(String.format(res.getString(R.string.rate_title),res.getString(R.string.app_name)));
        
    	
    	
        LinearLayout ll = new LinearLayout(mContext);
        ll.setOrientation(LinearLayout.VERTICAL);

        TextView tv = new TextView(mContext);
        tv.setText(String.format(res.getString(R.string.rate_phrase),res.getString(R.string.app_name)));
        tv.setWidth(240);
        tv.setPadding(4, 0, 4, 10);
        ll.addView(tv);

        Button b1 = new Button(mContext);
        b1.setText(String.format(res.getString(R.string.rate),res.getString(R.string.app_name)));
        b1.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + APP_PNAME)));
                editor.putBoolean("dontshowagain", true);
                dialog.dismiss();
            }
        });        
        ll.addView(b1);

        Button b2 = new Button(mContext);
        b2.setText(res.getString(R.string.remind_later));
        b2.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
            	editor.putLong("launch_count", 0);
            	editor.putLong("date_firstlaunch", System.currentTimeMillis());
            	long next_solved_count=new SolitairePuzzles(mContext).countSolvedPuzzles()+ADD_SOLVED_COUNT_ON_POSTPONE; 
            	editor.putLong("next_solved_count", next_solved_count);
            	editor.commit();
                dialog.dismiss();
            }
        });
        ll.addView(b2);

        Button b3 = new Button(mContext);
        b3.setText(res.getString(R.string.no_thanks));
        b3.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                if (editor != null) {
                    editor.putBoolean("dontshowagain", true);
                    editor.commit();
                }
                dialog.dismiss();
            }
        });
        ll.addView(b3);

        dialog.setContentView(ll);        
        dialog.show();        
    }
}