/*
 * Copyright 2012 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.google.android.gcm.demo.server;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.logging.Logger;

import org.lightcouch.CouchDbClient;
import org.lightcouch.DesignDocument;
import org.lightcouch.Response;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

/**
 * Simple implementation of a data store using standard Java collections.
 * <p>
 * This class is thread-safe but not persistent (it will lost the data when the
 * app is restarted) - it is meant just as an example.
 */
public final class Datastore {

  private static final List<String> regIds = new ArrayList<String>();
  private static final Logger logger =
      Logger.getLogger(Datastore.class.getName());

  static CouchDbClient dbClient = new CouchDbClient();
  //static Gson gson=new GsonBuilder().setDateFormat("yyyy/MM/dd HH:mm:ss Z").create();
  static SimpleDateFormat df=new SimpleDateFormat("yyyy/MM/dd HH:mm:ss Z");
  static{
	  DesignDocument designDoc=dbClient.design().getFromDesk("android_db");
	  dbClient.design().synchronizeWithDb(designDoc); // sync with db 
  }
  private Datastore() {
    throw new UnsupportedOperationException();
  }

  /**
   * Registers a device.
   */
  public static void register(String regId, Map<String,String>params) {
    synchronized (regIds) {
    	String _id=regId.trim();
    	params.put("_id", _id);
    	Response response;
    	if (!dbClient.contains(_id)){
    		params.put("created_at", df.format(new Date()));
    		response= dbClient.save(params);
    	}else{
    		Map<String,String> obj = dbClient.find(Map.class, _id);
    		params.put("modified_at", df.format(new Date()));
    		obj.putAll(params);
    		response= dbClient.update(obj);
    	}    			
      if (!regIds.contains(regId)){
    	  regIds.add(regId);
    	  logger.info("Registering " + regId);
      }else{
    	  logger.info("Already registered:" + regId);
      }
    	  
    }
  }

  /**
   * Registers a device.
   */
  public static void register(String regId) {
    synchronized (regIds) {
    	register(regId,new HashMap<String, String>());
    }
  }

  /**
   * Unregisters a device.
   */
  public static void unregister(String regId) {
    logger.info("Unregistering " + regId);
    synchronized (regIds) {
      regIds.remove(regId);
    }
  }

  /**
   * Updates the registration id of a device.
   */
  public static void updateRegistration(String oldId, String newId) {
    logger.info("Updating " + oldId + " to " + newId);
    synchronized (regIds) {
      regIds.remove(oldId);
      regIds.add(newId);
    }
  }

  /**
   * Gets all registered devices ids.
   */
  public static List<String> getDevices() {
    synchronized (regIds) {
    	List<JsonObject> list = dbClient.view("android_db/by_modify_time")
    			.descending(true).query(JsonObject.class);	
    ArrayList<String> ids=new ArrayList<String>();	
      for (JsonObject json:list){
    	  ids.add(json.get("value").getAsString());
      }
    return ids;	  
    }
  }
  
  /**
   * Gets all registered devices with infos.
   * for now - Info=last update date
   * key - device id, value - info as String.
   */
  public static List<MobileDevice> getDevicesInfo() {
    synchronized (regIds) {
    	List<JsonObject> list = dbClient.view("android_db/by_modify_time")
    			.descending(true).query(JsonObject.class);	
      List<MobileDevice> devs=new ArrayList<MobileDevice>();	
      for (JsonObject json:list){
    	  devs.add(new MobileDevice(json.get("value").getAsString(),json.get("key").getAsString()));
      }
    return devs;	  
    }
  }
}
