package com.google.android.gcm.demo.server;

import java.io.Serializable;

public class MobileDevice implements Serializable {

	public MobileDevice(String regId, String lastUpdate) {
		super();
		this.regId = regId;
		this.lastUpdate = lastUpdate;
	}
	/**
	 * 
	 */
	private static final long serialVersionUID = -5648442650658811154L;
	private String regId;
	private String lastUpdate;
	public String getRegId() {
		return regId;
	}
	public void setRegId(String regId) {
		this.regId = regId;
	}
	public String getLastUpdate() {
		return lastUpdate;
	}
	public void setLastUpdate(String lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	
	
}
