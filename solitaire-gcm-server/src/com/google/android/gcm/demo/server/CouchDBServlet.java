/*
 * Copyright 2012 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.google.android.gcm.demo.server;

import com.google.android.gcm.server.Constants;
import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.MulticastResult;
import com.google.android.gcm.server.Result;
import com.google.android.gcm.server.Sender;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.logging.Level;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.lightcouch.CouchDbClient;
import org.lightcouch.DesignDocument;
import org.lightcouch.Params;
import org.lightcouch.Response;

/**
 * Servlet that adds a new message to all registered devices.
 * <p>
 * This servlet is used just by the browser (i.e., not device).
 */
@SuppressWarnings("serial")
public class CouchDBServlet extends BaseServlet {



  private static final Executor threadPool = Executors.newFixedThreadPool(5);

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);
  }


  /**
   * Processes the request to add a new message.
   */
  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp)
      throws IOException, ServletException {
	String status="<h3>Testing Couchdb</h3>";
	// initialize a client instance - reuse
	CouchDbClient dbClient = new CouchDbClient();

	// CRUD API
	String ID="12348";
	Map<String, String> map = new HashMap<String, String>();
	map.put("_id", ID);
	map.put("name", "Alex");
	SimpleDateFormat df=new SimpleDateFormat("yyyy/MM/dd HH:mm:ss Z");  
	//Gson gson=new GsonBuilder().setDateFormat("yyyy/MM/dd HH:mm:ss Z").create();
	map.put("created_at", df.format(new Date()));
	Response response;
	if (!dbClient.contains(ID)) response= dbClient.save(map);
			
	map = dbClient.find(Map.class, ID);
	status+="Name= "+map.get("name");
	map.put("name", "AlexNew");
	map.put("modified_at", df.format(new Date()));
	dbClient.update(map);
	JsonObject json=dbClient.find(JsonObject.class, ID);
	status+="<br> New name="+json.get("name").getAsString()+"id= "+ json.get("_id").getAsString();
	if (dbClient.contains(ID)) status+="<br>"+ID+" exists";
	if (!dbClient.contains("999")) status+="<br>999 not exists";
	// view from pre-saved db
	/*List<JsonObject> list = dbClient.view("view_all/viewall").query(JsonObject.class);
	for(JsonObject json1:list){
		status+="<br>"+json1.get("value")+", last updated: "+json1.get("key");
	}*/
	// own view
	//dbClient.design().synchronizeAllWithDb();
	DesignDocument designDoc=dbClient.design().getFromDesk("android_db");
	dbClient.design().synchronizeWithDb(designDoc); // sync with db 
	List<JsonObject> list2 = dbClient.view("android_db/by_modify_time")
			.descending(true).query(JsonObject.class);
	for(JsonObject json1:list2){
		status+="<br>"+json1.get("value")+", last updated: "+json1.get("key");
	}
	//map = dbClient.find(Map.class, "doc-id", new Params().revsInfo());

	//dbClient.update(map);
	//dbClient.remove(map); 
    /*
	boolean b = dbClient.contains("doc-id");

	// attachments 
	dbClient.saveAttachment(instream, "photo.jpeg", "image/jpeg");
	InputStream in = dbClient.find("doc-id/photo.jpeg"); 


	// design documents stored on local .js files
	DesignDocument designDoc = dbClient.design().getFromDesk("example");  

	dbClient.design().synchronizeWithDb(designDoc); // sync with db 
	designDoc = dbClient.design().getFromDb("_design/example"); 

	// bulk 
	dbClient.bulk(newDocs, allOrNothing);

	List<Foo> bulkDocs = dbClient.view("_all_docs")
	  .includeDocs(true)
	  .keys(keysList)
	  .query(Foo.class);

	// Views
	List<Foo> list = dbClient.view("example/view")
	 	.key("key")
	 	.includeDocs(true)
	 	.query(Foo.class);
	 	
	// primitive types
	int count = dbClient.view("example/by_tag").key("couchdb").queryForInt(); 

	// Views pagination
	Page<Foo> page = dbClient.view("example/view").queryPage(...); 

	// all docs
	List<JsonObject> allDocs = dbClient.view("_all_docs").query(JsonObject.class);

	// temp Views
	List<Foo> list = dbClient.view("_temp_view").tempView("temp1").query(Foo.class);

	// update handlers
	String output = dbClient.invokeUpdateHandler("example/update", "doc-id", query);

	// database 
	CouchDbInfo info = dbClient.context().info();
	dbClient.context().createDB("new-db");  
	dbClient.context().compact();

	// replication
	ReplicationResult replication = dbClient.replication()
		.source("source-db").target("target-db")
		.createTarget(true)
		.trigger();

	// _replicator database      
	Replicator replicator = dbClient.replicator()
		.source("source-db").target("target-db")
		.filter("example/filter")
		.continuous(true);
	replicator.save(); // triggers a replication

	// Change notifications
	Changes changes = dbClient.changes()
		.includeDocs(true)
		.heartBeat(30000)
		.continuousChanges();

	while (changes.hasNext()) {
		// live access to continuous feeds
	}		 	

	*/
    req.setAttribute(HomeServlet.ATTRIBUTE_STATUS, status.toString());
    getServletContext().getRequestDispatcher("/home").forward(req, resp);
  }

 

}
