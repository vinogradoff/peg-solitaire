function(doc) {
  if (doc.modified_at)
 	emit(doc.modified_at, doc._id);
  else
  	emit(doc.created_at, doc._id);
}